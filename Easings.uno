using Uno.UX;
using Fuse.Animations;

namespace Fuse.Reactive
{
	[UXFunction("easing")]
	public sealed class Easing: UnaryOperator
	{
		[UXConstructor]
		public Easing([UXParameter("Value")] Expression value): base(value) {}
		protected override object Compute(object s)
		{
			string _easing = s.ToString();
			if (_easing == "BackIn") return Fuse.Animations.Easing.BackIn;
			if (_easing == "BackOut") return Fuse.Animations.Easing.BackOut;
			if (_easing == "BackInOut") return Fuse.Animations.Easing.BackInOut;
			if (_easing == "BounceIn") return Fuse.Animations.Easing.BounceIn;
			if (_easing == "BounceOut") return Fuse.Animations.Easing.BounceOut;
			if (_easing == "BounceInOut") return Fuse.Animations.Easing.BounceInOut;
			if (_easing == "CircularIn") return Fuse.Animations.Easing.CircularIn;
			if (_easing == "CircularOut") return Fuse.Animations.Easing.CircularOut;
			if (_easing == "CircularInOut") return Fuse.Animations.Easing.CircularInOut;
			if (_easing == "CubicIn") return Fuse.Animations.Easing.CubicIn;
			if (_easing == "CubicOut") return Fuse.Animations.Easing.CubicOut;
			if (_easing == "CubicInOut") return Fuse.Animations.Easing.CubicInOut;
			if (_easing == "ElasticIn") return Fuse.Animations.Easing.ElasticIn;
			if (_easing == "ElasticOut") return Fuse.Animations.Easing.ElasticOut;
			if (_easing == "ElasticInOut") return Fuse.Animations.Easing.ElasticInOut;
			if (_easing == "ExponentialIn") return Fuse.Animations.Easing.ExponentialIn;
			if (_easing == "ExponentialOut") return Fuse.Animations.Easing.ExponentialOut;
			if (_easing == "ExponentialInOut") return Fuse.Animations.Easing.ExponentialInOut;
			if (_easing == "Linear") return Fuse.Animations.Easing.Linear;
			if (_easing == "QuadraticIn") return Fuse.Animations.Easing.QuadraticIn;
			if (_easing == "QuadraticOut") return Fuse.Animations.Easing.QuadraticOut;
			if (_easing == "QuadraticInOut") return Fuse.Animations.Easing.QuadraticInOut;
			if (_easing == "QuarticIn") return Fuse.Animations.Easing.QuarticIn;
			if (_easing == "QuarticOut") return Fuse.Animations.Easing.QuarticOut;
			if (_easing == "QuarticInOut") return Fuse.Animations.Easing.QuarticInOut;
			if (_easing == "QuinticIn") return Fuse.Animations.Easing.QuinticIn;
			if (_easing == "QuinticOut") return Fuse.Animations.Easing.QuinticOut;
			if (_easing == "QuinticInOut") return Fuse.Animations.Easing.QuinticInOut;
			if (_easing == "SinusoidalIn") return Fuse.Animations.Easing.SinusoidalIn;
			if (_easing == "SinusoidalOut") return Fuse.Animations.Easing.SinusoidalOut;
			if (_easing == "SinusoidalInOut") return Fuse.Animations.Easing.SinusoidalInOut;
			// as a fallback, always return Linear
			return Fuse.Animations.Easing.Linear;
		}

		public override string ToString()
		{
			return "easing(" + Operand + ")";
		}
	}
}
